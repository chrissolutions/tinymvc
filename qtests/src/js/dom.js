/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* globals console, window */

/**
 * DOM helper class.
 *
 * @class
 */
class Dom {

    /**
     * Return the html node.
     */
    get node() {
        return this._node;
    }

    /**
     * Return the element.
     */
    get element() {
        return this._element;
    }

    /**
     * Get the base URI.
     */
    get baseUri() {
        return this.node.baseURI;
    }

    /**
     * Get the uri root path.
     */
    get rootUri() {
        return Dom.getRootUri(this.baseUri);
    }

    /**
     * Get the child elements.
     */
    get children() {
        return Dom.children(this.node);
    }

    /**
     * Get number of elements.
     */
    get length() {
        return this._element instanceof NodeList 
            ? this.element.length
            : 1;
    }

    /**
     * Get the inner text.
     * 
     * @returns {String}                inner text.
     */
    get text() {
        return this.node.innerText;
    }

    /**
     * Set the inner text.
     * 
     * @param {String|Number} value     text value.
     */
    set text(value) {
        this.node.innerText = value;
    }

    /**
     * Get the value.
     * 
     * @returns {*}                     element value;
     */
    get value() {
        switch (this.element.tagName.toLowerCase()) {
            case "input":
                switch (this.element.type.toLowerCase()) {
                    case "checkbox":
                        return this.element.checked;

                    default:
                        break;
                }
                break;

            default:
                break;
        }

        return this.element.value;
    }

    /**
     * Set the value.
     * 
     * @param {String|Number} value     text value.
     */
    set value(value) {
        switch (this.element.tagName.toLowerCase()) {
            case "input":
                switch(this.element.type.toLowerCase()) {
                    case "checkbox":
                        this.element.checked = value;
                        break;

                    default:
                        break;
                }
                break;

            default:
                break;
        }

        this.element.value = value;
    }

    /**
     * Constructor
     * 
     * @param {*} selector              element selector.
     * @param {*} context               element context.
     */
    constructor(selector, context=document) {
        this._element = !selector 
            ? Dom.create().element
            : typeof selector === "string" 
            ? Dom.find(selector, context)
            : selector instanceof Dom
                ? selector.element
                : selector;

                this._node = this._element instanceof HTMLElement 
                    || this._element instanceof NodeList
                    || this._element instanceof DocumentFragment
                        ? this._element
                        : document;
    }

    /**
     * Add css class to node.
     * 
     * @param {String} className        Css class name.
     */
    addClass(className) {
        Dom.addClass(this.node, className);
    }

    /**
     * Append a child node.
     * 
     * @param {Dom} node                child node.
     */
    appendChild(node) {
        const element = node instanceof Dom ? node.node : node;
        const child = element instanceof DocumentFragment
            ? element.firstElementChild
            : element;
        Dom.appendChild(this.node, child);
    }

    /**
     * Remove all child nodes.
     */
    empty() {
        return Dom.as(Dom.empty(this.node));
    }

    /**
     * Find element or elements matching the selector.
     * 
     * @param {*} selector              element selector.
     * @returns {Dom}                   Dom object.
     */
    find(selector) {
        return Dom.as(Dom.find(selector, this.node));
    }

    /**
     * Find input elements.
     * 
     * @returns {Dom}                   Dom object.
     */
    findInputElements() {
        return Dom.as(Dom.findInputElements(this.node));
    }

    /**
     * Find the computed style.
     * 
     * @param {String} style            CSS style id. 
     * @returns {*}                     computed style value.
     */
    findStyle(style) {
        return Dom.findStyle(this.node, style);
    }

    /**
     * Add event listener.
     * 
     * @param {string} eventId          event identifier.
     * @param {function} callback       callback function. 
     */
    on(eventId, callback) {
        this.element.addEventListener(eventId, callback);
    }

    /**
     * Remove event listener.
     * 
     * @param {string} eventId          event identifier.
     * @param {function} callback       callback function. 
     */
    off(eventId, callback) {
        this.element.removeEventListener(eventId, callback);
    }

    /**
     * Attach to click event.
     * 
     * @param {function} callback       callback function. 
     */
    onclick(callback) {
        this.on("click", (event) => { callback(this, event); });
    }

    /**
     * Attach to resize event.
     * 
     * @param {function} callback       callback function. 
     */
    onresize(callback) {
        Dom.onresize(this.node, callback);
    }

    /**
     * Set the element width & height.
     * 
     * @param {int} width               element width in pixels
     * @param {int} height              element height in pixels
     */
    setSize(width, height) {
        this.element.style.width = width + "px";        
        this.element.style.height = height + "px";        
    }

    /**
     * Show/Hide node
     * 
     * @param {*} mode                  true: show, false: hide
     */
    show(mode) {
        if (mode === undefined || mode) {
            this.node.style.display = "block";
        } else {
            this.node.style.display = "none";
        }
    }

    /**
     * Add css class to node.
     * 
     * @param {Dom} node                Dom node.
     * @param {String} className        Css class name. 
     */
    static addClass(node, className) {
        if (node.className.indexOf(className) === -1) {
            node.className = `${node.className} ${className}`;
        }
    }

    /**
     * Append a child node.
     * 
     * @param {Dom} node                Dom node.
     * @param {HTMLElement} childNode   child element.
     */
    static appendChild(node, childNode) {
        node.appendChild(childNode);
    }

    /**
     * Convert element to Dom object.
     * 
     * @param {HTMLElement} element     HTML element.
     * @returns {Dom}                   Dom object.
     */
    static as(element) {
        return new Dom(element);
    }

    /**
     * Returns an array of node's children
     * 
     * @param {Dom} node                Dom node.
     * @returns {Array}                 Array of child elements.
     */
    static children(node) {
        return Array.from(node.children);
    }

    /**
     * Create Dom object from html string.
     * 
     * @param {string} html             HTML string.
     * @returns {Dom}                   Dom object.
     */
    static create(html) {
        if (!html) {
            var container = document.createElement("DIV");
            container.style.width = "100%";
            container.style.height = "100%";
            container.style.position = "relative";
            return Dom.as(container);
        } else {
            return Dom.as(Dom.parseHTML(html));
        }
    }

    /**
     * Empties a node of all of its children.
     * 
     * @param {Dom} node                Dom node.
     */
    static empty(node) {
        while (node.firstChild) {
            node.removeChild(node.firstChild);
        }
        return node;
    }

    /**
     * Find element or elements matching the selector.
     * 
     * @param {*} selector              element selector.
     * @param {*} context               element context.
     * @return {HTMLElement}            html element(s).
     */
    static find(selector, context) {
        try {
            switch (selector.charAt(0)) {
                case "#":
                    return Dom.findOne(selector, context);

                case ".": {
                    const results = Dom.findAll(selector, context);
                    return results.length === 1 ? results[0] : results;
                }

                default:
                    return Dom.findOne(`#${selector}`, context);
            }
        } catch (e) {
            return null;
        }
    }

    /**
     * Find first element matching the selector.
     * 
     * @param {*} selector              element selector.
     * @param {*} context               element context.
     * @return {HTMLElement}            html element.
     */
    static findOne(selector, context) {
        try {
            return (context || document).querySelector(selector);
        } catch (e) {
            return null;
        }
    }

    /**
     * Find all elements matching the selector.
     * 
     * @param {*} selector              element selector.
     * @param {*} context               element context.
     * @return {Array}                  array of html elements.
     */
    static findAll(selector, context) {
        try {
            return (context || document).querySelectorAll(selector);
        } catch (e) {
            return null;
        }
    }

    /**
     * Find input elements from the context.
     * 
     * @param {*} context               element context.
     */
    static findInputElements(context) {
        try {
            const results = (context || document).getElementsByTagName("input");
            return results.length === 1 ? results[0] : results;
        } catch (e) {
            return null;
        }
    }

    /**
     * Find the computed style.
     * 
     * @param {HTMLElement} element     HTML element.
     * @param {String} style            CSS style id. 
     * @returns {*}                     computed style value.
     */
    static findStyle(element, style) {
        return window.getComputedStyle(element).getPropertyValue(style);
    }

    /**
     * Return root of the uri.
     * 
     * @param {string} uri              uri.
     * @returns {string}                root uri.
     */
    static getRootUri(uri) {
        if (!uri) return "";
        const url = new URL(uri);
        let parts = url.pathname.split("/");
        parts = parts.slice(0, 2 - (parts.length === 2));
        parts[0] = url.origin;
        return parts.join("/");
    }    

    /**
     * Add event listener to node.
     * 
     * @param {Dom} node                Dom node.
     * @param {string} eventId          Event id. 
     * @param {function} callback       callback function.           
     */
    static on(node, eventId, callback) {
        node.addEventListener(eventId, callback);
    }

    /**
     * Remove event listener from node.
     * 
     * @param {Dom} node                Dom node.
     * @param {string} eventId          Event id.
     * @param {function} callback       callback function.
     */
    static off(node, eventId, callback) {
        node.removeEventListener(eventId, callback);
    }

    /**
     * Set resize event handler.
     * 
     * @param {Dom} node                Dom node.
     * @param {function} callback       callback function.
     */
    static onresize(node, callback) {
        if (!window.resizer) {
            window.resizer = {
                resizing: false,
                listeners: [],
                run: () => {
                    window.resizer.listeners.forEach(listener => listener(node));
                    window.resizer.resizing = false;
                }
            };
            window.addEventListener("resize", () => {
                if (!window.resizer.resizing) {
                    window.resizer.resizing = true;
                    if (window.requestAnimationFrame) {
                        window.requestAnimationFrame(window.resizer.run);
                    } else {
                        setTimeout(window.resizer.run, 66);
                    }
                }
            });
        }

        if (callback) {
            window.resizer.listeners.push(callback);
        }
    }

    /**
     * Parse HTML text into a document fragment.
     * 
     * @param {string} text             HTML text.
     * @param {HTMLElement} context     HTML element context.
     * @returns {HTMLDocumentFragment}  HTML document fragement.
     */
    static parseHTML(text, context) {
        context = context || document;

        const rhtml = /<|&#?\w+;/;
        const fragment = context.createDocumentFragment();

        if (!rhtml.test(text)) {
            fragment.appendChild(context.createTextNode(text));

            // Convert html into DOM nodes
        } else {
            let tmp = fragment.appendChild(context.createElement("div"));

            // Deserialize a standard representation
            const rtagName = /<([\w:]+)/;
            const tag = (rtagName.exec(text) || ["", ""])[1].toLowerCase();

            // We have to close these tags to support XHTML (#13200)
            const wrapMap = {
                // Support: IE9
                option: [1, "<select multiple=\"multiple\">", "</select>"],

                thead: [1, "<table>", "</table>"],
                col: [2, "<table><colgroup>", "</colgroup></table>"],
                tr: [2, "<table><tbody>", "</tbody></table>"],
                td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],

                _default: [0, "", ""]
            };
            const wrap = wrapMap[tag] || wrapMap._default;
            const rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi;
            tmp.innerHTML = wrap[1] + text.replace(rxhtmlTag, "<$1></$2>") + wrap[2];

            // Descend through wrappers to the right content
            let j = wrap[0];
            while (j--) {
                tmp = tmp.lastChild;
            }

            // Remove wrappers and append created nodes to fragment
            fragment.removeChild(fragment.firstChild);
            while (tmp.firstChild) {
                fragment.appendChild(tmp.firstChild);
            }
        }

        return fragment;
    }
}
