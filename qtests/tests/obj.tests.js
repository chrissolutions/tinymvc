/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* globals console, define, Obj, Kit */
(function () {
    "use strict";
    
    var dependencies = [
            "qunit",
            "obj"
        ];
    
    // Unit test definitions.
	define(dependencies, function(Qunit) {
        
		// Define the QUnit module and lifecycle.
		Qunit.module("Obj tests", { 
			setup: function () {
			},
			teardown: function () {
			}
		});
        
        Qunit.test("test convert value", function(assert) {
            assert.equal(500, Obj.convert("500", "number"));
            assert.equal(500, Obj.convert("500", "int"));
            assert.equal(50.2, Obj.convert("50.20", "float"));
            assert.equal("Hello World", Obj.convert("Hello World", "string"));

            assert.equal(false, Obj.toBoolean("0"));
            assert.equal(true, Obj.toBoolean("1"));
            assert.equal(500, Obj.toNumber("500"));
            assert.equal(500, Obj.toInt("500", "int"));
            assert.equal(50.2, Obj.toFloat("50.20", "float"));
            assert.equal(-500, Obj.toNumber("-500"));
        });
    });
}());
