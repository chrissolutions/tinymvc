/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* globals console, document, window, Dom */
// ReSharper disable InconsistentNaming

/**
 * Wrapper class for window.
 *
 * @class
 */
const Window = (() => {

    const _referrer = document.referrer,
        _hostUri = Dom.getRootUri(_referrer);

    class Window {

        /**
         * Return the html node.
         */
        static get node() {
            return Window.element.document;
        }

        /**
         * Return the element.
         */
        static get element() {
            return document.defaultView || window;
        }

        /**
         * Get the base URI.
         */
        get baseUri() {
            return Window.node.baseURI;
        }

        /**
         * Get the root of host URI.
         */
        static get hostUrl() {
            return _hostUri;
        }

        /**
         * Get number of elements.
         */
        static get length() {
            return Window.element.length;
        }

        /**
         * Attach to hashchange event.
         * 
         * @param {*} callback          event handler. 
         */
        static onhashchange(callback) {
            Dom.on(Window.element, "hashchange", () => { callback(this.element); });
        }

        /**
         * Attach to load event.
         * 
         * @param {*} callback          event handler.
         */
        static onload(callback) {
            Dom.on(Window.element, "load", () => { callback(this.element); });
        }

        /**
         * Attach to beforeunload event.
         * 
         * @param {*} callback          event handler.
         */
        static onbeforeunload(callback) {
            Dom.on(Window.element, "beforeunload", (event) => callback(this.element, event));
            Dom.on(Window.element, "onbeforeunload", (event) => callback(this.element, event));
        }

        /**
         * Attach to message event.
         * 
         * @param {*} callback          event handler.
         */
        static onmessage(callback) {
            Dom.on(Window.element, "message", (event) => callback(this.element, event));
        }

        /**
         * Attach to beforeunload event.
         * 
         * @param {*} callback          event handler.
         */
        static onresize(callback) {
            Dom.onresize(Window.element, callback);
        }
    }

    return Window;

})();
