/*jshint strict: true, undef: true */
/*globals $, console, document, delegate */

const Publisher = (() => {
    const getDelegate = (callback) => {
        return !callback.scope ? delegate(null, callback) : callback;
    };

    class Publisher {
        constructor(message) {
            this._message = message;
            this._subscribers = [];
        }

        /**
         * message
         *
         * getter retrieving the associated message.
         *
         * @param {string}  message     Message name.
         */    
        get message() { 
            return this._message;
        }

        /**
         * subsribe(subscriber)
         *
         * adds a subscriber.
         *
         * @param {function}    subscriber  Event subscriber.
         */    
        subscribe(subscriber) {
            this._subscribers.push(getDelegate(subscriber));
        }

        /**
         * unsubsribe(subscriber)
         *
         * removes a subscriber
         *
         * @param {function}    subscriber  Event subscriber.
         */    
        unsubscribe(subscriber) {
            var subscriberDelegate = getDelegate(subscriber);
            if (subscriberDelegate !== null) {
                let newArray = [];
                this._subscribers.forEach(delegate => {
                    if (delegate.equals(subscriberDelegate) === false) {
                        newArray.push(delegate);
                    }
                });
                
                this._subscribers = newArray;
            }
        }
        
        /**
         * publish([arg1[, arg2[, ...]]])
         *
         * triggers event to subscribers.
         *
         * @param {Arguments}   arguments   Event arguments.
         */    
        publish() {
            let retval = null;
            this._subscribers.forEach(subscriber => {
                if (subscriber) {
                    retval = subscriber.apply(subscriber, Array.from(arguments));
                }
            });
            return retval;
        }

        /**
         * clear()
         *
         * removes all subscribers.
         */    
        clear() {
            this._subscribers = [];
        }

        /**
         * hasSubscribers()
         *
         * checks for any subscribers.
         */    
        hasSubscribers() {
            return this._subscribers.length > 0;
        }
    }
  
    return Publisher;
  })();
