/*jshint strict:true, undef:true, eqeqeq:true, laxbreak:true */

/**
 * The glossary used to hold display strings.
 *
 * @class
 */
class Glossary {

    /**
     * Constructor
     * 
     * @param {*} entries               glossary entries. 
     */
    constructor(entries) {
        if (entries) {
            this.init(entries);
        }
    }

    /**
     * Initialize the glossary
     * 
     * @param {Object|Array} entries    glossary entries. 
     */
    init(entries) {
        var self = this;

        if (Array.isArray(entries)) {
            entries.forEach(function(item) {
                Object.defineProperty(self, item.key, {
                    get: function() { return item.value; },
                    enumerable: true,
                    configurable: false
                });
            });
        } else {
            Object.getOwnPropertyNames(entries).forEach(function(name) {
                Object.defineProperty(self, name, {
                    get: function() { return entries[name]; },
                    enumerable: true,
                    configurable: false
                });
            });
        }
    }
}
