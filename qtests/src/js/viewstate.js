/* jshint strict: true, undef: true, eqeqeq: true */
/* eslint no-unused-vars: 0*/
/* global pako, TextDecoder, TextEncoder, Obj */
// ReSharper disable PossiblyUnassignedProperty

/**
 * ViewState
 *
 * @class
 */
const ViewState = (() => {

    /**
     * Decode binary string of zipped data into a view state object.
     * 
     * @param {string} bstr             binary string.
     * @return {*}                      view state object.
     */
    const decode = bstr =>  {
        if (!bstr) return null;
        const data = Uint8Array.from(atob(bstr).split(","), e => Number(e));
        const unzipped = pako.inflate(data);
        return JSON.parse(new TextDecoder("utf-8").decode(unzipped));
    };

    /**
     * Encode the view state object into a binary string of zipped data.
     * 
     * @param {*} obj                   view state object.
     * @return {string}                 binary string.
     */
    const encode = obj => {
        if (!obj || Obj.isEmpty(obj)) return null;
        const binData = new TextEncoder("utf-8").encode(JSON.stringify(obj));
        const zipped = pako.deflate(binData);
        return btoa(zipped);
    };

    /**
     * Viewstate class
     */
    class ViewState {

        /**
         * ViewState constructor
         * 
         * @param {*} key               view state key.
         * @param {*} value             view state value.
         */
        constructor(key=null, value=null) {
            if (Obj.isString(key)) {
                this._key = key;
                this._value = value;
            } else if (Obj.isObject(key)) {
                this._key = key.key;
                this._value = key.value;
            }
        }

        /**
         * key getter.
         *
         * @returns {String}            Name of the view.
         */
        get key() {
            return this._key;
        }

        /**
         * value getter.
         *
         * @returns {String}            Name of the view.
         */
        get value() {
            return this._value;
        }

        /**
         * hasValue.
         *
         * @returns {boolean}           true: view state has a value; false otherwise.
         */
        hasValue() {
            return this.value || false;
        }

        /**
         * Deserialze viewstate.
         * 
         * @returns {*}                 deserialized view state value json object. 
         */
        deserialize() {
            return ViewState.deserialize(this);
        }

        /**
         * Serialize viewstate.
         * 
         * @returns {*}                 serialized view state value object. 
         */
        serialize() {
            return ViewState.serialize(this);
        }

        /**
         * Deserialize binary string of zipped data into a view state object.
         * 
         * @param {*} viewstate         view state 
         * @returns {*}                 deserialized view state json object. 
         */
        static deserialize(viewstate) {
            viewstate._value = Obj.isString(viewstate.value) ? decode(viewstate.value) : viewstate.value;
            return viewstate;
        }

        /**
         * Serialize the view state object into a binary string of zipped data.
         * 
         * @param {*} viewstate         view state 
         * @returns {*}                 serialized view state json object. 
         */
        static serialize(viewstate) {
            viewstate._value = Obj.isString(viewstate.value) ? viewstate.value : encode(viewstate.value);
            return viewstate;
        }
    }
  
    return ViewState;
})();
