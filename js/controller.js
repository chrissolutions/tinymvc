/* jshint strict: true, undef: true, eqeqeq: true */
/* eslint no-unused-vars: 0*/
/* globals console */
// ReSharper disable UnusedParameter

/**
 * Controller class.
 *
 * @class
 */
class Controller {

    /**
     * Main view.
     */
    get mainView() {
        return this._mainView;
    }

    /**
     * Model getter.
     *
     * @returns {*}                 Model.
     */
    get model() {
        return this._model;
    }

    /**
     * Model setter.
     * 
     * @param {*} value             Model.
     */
    set model(value) {
        return this._model = value;
    }

    /**
     * Setting object.
     */
    get settings() {
        return this._mainView.settings;
    }

    /**
     * Event subscribers.
     */
    get subscribers() {
        return this._subscribers;
    }

    /**
     * Constructor
     * 
     * @param {*} mainView              main view.
     */
    constructor(mainView) {
        this._mainView = mainView;
        this._model = null;
        this._state = [];
        this._currentState = null;
        this._prevState = null;
        this._subscribers = Object.create(null);
    }

    /**
     * Register subscribers 
     * 
     * @param {*} subscribers           event subscribers. 
     */
    register(subscribers) {
        this._subscribers = Object.assign(this._subscribers, subscribers);
    }

    /**
     * Run the controller.
     * 
     * @param {*} args                  arguments.
     */
    run(...args) {
        this.mainView.on(this.subscribers);
        this.mainView.render(this.mainView.commands.loadContent, ...args);
    }

    /**
     * Navigate to the controller state.
     *
     * @param {string} location     uri route location
     */
    navigate(location) {
        this._currentState = this.onNavigate(location);
        if (this._prevState !== this._currentState) {
            this.onDisplay();
        }
        this._prevState = this._currentState;
    }

    /**
     * message sent from host window.
     * 
     * @param {*} ctx                   windows context.
     * @param {Event} event             event object.
     */
    message(ctx, event) {
        if (event.data === "resize") {
            this.onResize(ctx);
        } else {
            this.onMessage(event);
        }
    }

    /**
     * Resize main window
     * 
     * @param {*} ctx                   window context.
     */
    resize(ctx) {
        this.onResize(ctx);
    }

    /**
     * Load the controller.
     * 
     * @param {*} ctx                   window context.
     */
    load(ctx) {
        this.navigate(ctx.location);
    }

    /**
     * Unload the controller.
     * 
     * @param {*} ctx                   windows context.
     * @param {Event} event             event object.
     * @returns {*}                     continuation status.
     */
    unload(ctx, event) {
        console.log("unload: kilroy");
        return null;
    }

    /**
     * onDisplay event.
     */
    onDisplay() {
    }

    /**
     * onMessage handler.
     * 
     * @param {Event} event             Event object.
     */
    onMessage(event) {
        console.log(`message received: '${event.data}' from '${event.target.location}'`);
    }

    /**
     * onNavigate event
     * 
     * @param {*} location              location object.
     * 
     * @returns {String} new controller state.
     */
    onNavigate(location) {
        const state = location.hash ? location.hash.split("/")[1] : "default";
        return state || "default";
    }

    /**
     * onResize event.
     * 
     * @param {*} ctx                   window context.
     */
    onResize(ctx) {
        this.mainView.resize(ctx);
    }
}
