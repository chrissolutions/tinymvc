/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* globals console, define, Kit */
(function () {
    "use strict";
	
	class Entry {
		constructor(id, title) {
			this.id = Obj.uuid();
			this.title = "";
			
			switch (arguments.length)
			{
				case 1:
					this.title = id;
					break;
					
				case 2:
					this.id = id;
					this.title = title;
					break;
					
				default:
					break;
			}
		}
	}
    
    var dependencies = [
            "qunit",
            "kit",
			"storage"
        ],
		
        establishStore = async function(storage, name) {
            let items = [
				"Bruce Wayne",
				"Clark Kent",
				"Barry Allen",
				"Hal Jordan",
				"Robert Kilroy"
			];
			
			await storage.drop(name);
            let store = await storage.create(name);
			items.forEach(item => store.setItem(new Entry(item)));
			await store.save();
			return store;
        };
		
		
    // Unit test definitions.
	define(dependencies, function(Qunit) {
        
		// Define the QUnit module and lifecycle.
		Qunit.module("Storage tests", { 
			setup: function () {
			},
			teardown: function () {
			}
		});
        
        Qunit.test("create and establish the storage", async function( assert ) {
			const storeName = "teststore";
            let storage = new Storage();
			await establishStore(storage, storeName);
			const items = await storage.getItems(storeName); 
            assert.equal(items.length, 5);
        });
		
        Qunit.test("get items; find item", async function( assert ) {
			const storeName = "teststore";
			
            var storage = new Storage();
			await establishStore(storage, storeName);
            var items = await storage.getItems(storeName);
            assert.equal(items.length, 5);
			
			var item = await storage.getItem(storeName, items[1].key);
			assert.equal(item.title, "Clark Kent");
        });
		
        Qunit.test("test store:  get items", async function( assert ) {
			const storeName = "teststore";
			
            let storage = new Storage();
			let store = await establishStore(storage, storeName);
            var items = await store.getItems();
            assert.equal(items.length, 5);
			
			var item = await store.getItem(items[1].key);
			assert.equal(item.title, "Clark Kent");
        });
		
        Qunit.test("test store:  find item", async function( assert ) {
			const storeName = "teststore";
			
            let storage = new Storage();
			let store = await establishStore(storage, storeName);
            let items = await store.getItems();
            assert.equal(items.length, 5);
			
			items = await store.find({title: "Clark Kent"});
			assert.equal(items[0].title, "Clark Kent");
			
			items = await store.findAll();
            assert.equal(items.length, 5);
        });
    });
}());
