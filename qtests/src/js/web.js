/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* eslint no-console: off */
/* globals console, fetch, Obj */
// ReSharper disable PossiblyUnassignedProperty

/**
 * The base model class.
 *
 * @class
 */
const Web = (() => {

    class Ajax {

        /**
         * Get query string from parameter object.
         * @param {*} parameters        parameter object.
         * @returns {string}            query string.
         */
        static getQueryString(parameters) {
            let queryString = "";

            if (parameters && !Obj.isEmpty(parameters)) {
                queryString = "?";
                Object.getOwnPropertyNames(parameters).forEach(name => {
                    queryString += `${name}=${parameters[name]}&`;
                });
                queryString = queryString.slice(0, -1);
            }

            return queryString;
        }

        /**
         * Parse request arguments.
         * 
         * @param {*} url               request url.
         * @param {*} options           request options.    
         * @returns {*}                 parsed options object.
         */
        static parseOptions(url, options) {
            const opts = Object.create(null);
            if (typeof url === "string") {
                opts.url = url;
                opts.options = options || Object.create(null);
            } else if (url instanceof Object) {
                opts.options = url;
                opts.url = options.url;
            } else {
                throw new Error("Argument error");
            }

            return opts;
        }

        /**
         * Obtain default headers.
         * 
         * @param {*} opts              options.
         * @return {*}                  default header.
         */
        static defaultHeaders(opts) {
            switch (opts.method) {
                case "get": 
                case "delete":
                    return {"Content-Type": "application/json; charset=UTF-8"};

                case "post": 
                case "put": {
                    let headers = {"Content-Type": "application/json; charset=UTF-8", "Accept": "application/json"};
                    if (opts.mode === "cors") {
                        headers = Object.assign({"Access-Control-Allow-Origin": "*"}, headers);
                    }
                    return headers;
                }

                default:
                    return null;
            }
        }

        /**
         * Perform web request.
         *
         * @param {*} method            request method.
         * @param {*} url               request url.
         * @param {*} options           request options.
         * @returns {*}                 response object.
         * 
         * @example
         * request(method, url)
         * request(method, url, options)
         * request(method, options)
         */
        static async request(method, url, options) {
            const args = Ajax.parseOptions(url, options);
            const opts = args.options;
            opts.method = method;
            opts.url = args.url;
            opts.mode = opts.mode || "cors";
            opts.credentials = opts.credentials || "include";
            opts.cache = opts.cache || "default";
            opts.headers = Object.assign(Ajax.defaultHeaders(opts), opts.headers);
            if (opts.data) {
                opts.body = JSON.stringify(opts.data);
            }
            opts.dataType = opts.dataType || "json";

            try {
                // issue web request.
                const response = await fetch(args.url, opts);
                if (response.status >= 200 && response.status < 300) {
                    switch(opts.dataType) {
                        case "json":
                            return await response.json();
                            
                        case "text":
                            return await response.text();

                        case "blob":
                            return await response.blob();

                        default:
                            return Object.assign({}, response, {success: false, errorMessage: `${opts.dataType} is an unsupported data type`});
                    }
                } else {
                    return Object.assign({}, response, {success: false, errorMessage: response.statusText});
                }
            } catch (e) {
                return { status: 503, statusText: e.message, success: false, errorMessage: "Network Error"};
            }
        }   
    }        
    
    class Web {

        /**
         * HTTP get request.
         * 
         * @param {string} url          request url.
         * @param {*} parameters        request parameters.
         * @param {*} options           request options.
         * @param {string} [options.url]                        request url.
         * @param {string} [options.dataType="json|text|blob"]  request type header text.
         * @param {string} [options.mode="cors|no-cors"]        request authentication mode.
         * @returns {*}                 response object.
         */
        static async get(url, parameters, options) {
            return await Ajax.request("get", url+Ajax.getQueryString(parameters), options);
        }

        /**
         * HTTP post request.
         * 
         * @param {*} url               request url.
         * @param {*} data              request data.
         * @param {*} options           request options.
         * @param {string} [options.url]                        request url.
         * @param {string} [options.dataType="json|text|blob"]  request type header text.
         * @param {string} [options.mode="cors|no-cors"]        request authentication mode.
         * @returns {*}                 response object.
         */
        static async post(url, data, options) {
            const opts = Object.assign(options || {});
            opts.data = data;
            return await Ajax.request("post", url, opts);
        }

        /**
         * HTTP put request.
         * 
         * @param {*} url               request url.
         * @param {*} data              request data.
         * @param {*} options           request options.
         * @param {string} [options.url]                        request url.
         * @param {string} [options.dataType="json|text|blob"]  request type header text.
         * @param {string} [options.mode="cors|no-cors"]        request authentication mode.
         * @returns {*}                 response object.
         */
        static async put(url, data, options) {
            const opts = Object.assign(options || {});
            opts.data = data;
            return await Ajax.request("put", url, opts);
        }

        /**
         * HTTP delete request.
         * 
         * @param {*} url               request url.
         * @param {*} options           request options.
         * @param {string} [options.url]                        request url.
         * @param {string} [options.dataType="json|text|blob"]  request type header text.
         * @param {string} [options.mode="cors|no-cors"]        request authentication mode.
         * @returns {*}                 response object.
         */
        static async delete(url, options) {
            return await Ajax.request("delete", url, options);
        }
    }

	return Web;

})();
