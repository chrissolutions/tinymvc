/*jshint strict: true, undef: true, laxbreak:true */
/* globals console, document, HTMLElement, Handlebars, Dom, Web */

/**
 * The base template class.
 *
 * @class
 */
const Template = (() => {

    /**
     * Load the template cache from the source properties.
     *
     * @param {*} self                  template object.           
	 * @param {object} source           source object.
     */
    function loadTemplateFromObject(self, source) {
        Object.getOwnPropertyNames(source).forEach(name => {
            self._templateCache[name] = Handlebars.compile(source[name]);
            self[name] = name;
        });
    }
    
    /**
     * Load the template cache from the DOM.
     * 
     * @param {*} self                  template object.
	 * @param {HTMLElement} source      DOM element containing templates.
     */
    function loadTemplateFromNode(self, source) {
        Dom.children(source).forEach(node => {
            var name = node.getAttribute("id").replace("template-", "");
            self._templateCache[name] = Handlebars.compile(node.innerHTML);
            self[name] = name;
        });
    }
    
    /**
     * Load templates from url.
     *
     * @param {*} self                  template object.
	 * @param {string} source           The url of the tmeplates.
     */
    async function loadTemplateFromUrl(self, source) {
        const lastSeparator = source.lastIndexOf(".");
        const addr = source.substr(0, lastSeparator);
        const ext = source.substr(lastSeparator);

        // load template file.
        const response = await Web.get(addr + (ext || ".html"), null, {dataType: "text"});

        // find the template section.
        let templateSection = Dom.find("#template-section");
        if (!templateSection) {
            templateSection = document.createElement("section");
            templateSection.setAttribute("id", "template-section");
        }

        templateSection.appendChild(Dom.create(response).element);
        Dom.children(templateSection).forEach(node => {
            const name = node.getAttribute("id").replace("template-", "");
            self._templateCache[name] = Handlebars.compile(node.innerHTML);
            self[name] = name;
        });
        
        Dom.empty(templateSection);
    }

    /***
     * @class
     * @classdesc   template class
     */
    class Template {
        constructor() {
            this._templateCache = Object.create(null);
        }

        /**
         * Initialize the template
         *
         * @param {HTMLElement|object|string} source    template source.
         * @returns {*}                 template object.
         * 
         * @example
         * // init templates from HTML element or object.
         * await template.init('#templates');
         * 
         * @example
         * // init from web service.
         * await template.init('http://webservice.com/templates');
         */
        async init(source) {
            return await this.loadTemplate(source);
        }

        /**
         * Create text using the named template.
         *
         * @param {string} name         template name
         * @param {object} data         template data
         * @returns {string}            text
         */
        createTextFor(name, data) {
            if (!name) return "";
            const template = this.getTemplate(name);
            return template(data);    
        }
        
        /**
         * Create element using the named template.
         *
         * @param {string}  name            template name
         * @param {object}  data            template data
         * @returns {HTMLElement}           HTML element
         */
        createNodeFor(name, data) {
            const html = this.createTextFor(name, data);
            const d = document.createElement("DIV");
            d.innerHTML = html;
            return d.childElementCount > 1 ? d : d.firstElementChild;
        }

        /**
         * Retreives the template by name.
         *
         * @param {string} name         template name.
         * @returns {*}                 compiled template.
         */
        getTemplate(name) {
            return this._templateCache[name];
        }

        /**
         * Load template
         * 
         * @param {*} source            template source.
         * @returns {*}                 template object.
         */
        async loadTemplate(source) {
            if (source instanceof HTMLElement) {
                loadTemplateFromNode(this, source);
            } else if (typeof source === "string") {
                var node = Dom.find(source);
                if (node) {
                    loadTemplateFromNode(this, node);
                } else {
                    await loadTemplateFromUrl(this, source);
                }
            } else {
                loadTemplateFromObject(this, source);
            }

            return this;
        }
    }
  
    return Template;
})();
