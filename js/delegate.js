/*jshint strict:true, undef:true, eqeqeq:true, laxbreak:true */
/* globals delegate */

/**
 * Creates a delegate.
 *
 * A delegate is a callback function when invoked its this variable is set to the
 * predefined scope object.
 *
 * NOTE: This class was written years before bind was natively supported by Javascript
 * functions.  However this class remains useful as bind doesn't support the ability
 * to inspect the scope object bound to the callback.
 *
 * @param {object}      scope       object used to set the callback's scope.
 * @param {function}    callback    the callback function.
 *
 * @returns {function}  delegate invocation function.
 *
 * @example
 * delegate(this, function([arg1[, arg2[, ...]]]) {
 *     ...
 * });
 */
(function (global) {
    global.delegate = (scope = global, callback = () => { }) => {
        this.invoke = function () {
            return (arguments) ? callback.apply(scope, arguments) : callback.apply(scope);
        };

        Object.defineProperty(this.invoke, "scope", {
            get() { return scope; },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(this.invoke, "callback", {
            get() { return callback; },
            enumerable: true,
            configurable: true
        });

        this.invoke.equals = function (delegate) {
            return this.scope === delegate.scope && this.callback === delegate.callback;
        };

        return this.invoke;
    };
})(window);