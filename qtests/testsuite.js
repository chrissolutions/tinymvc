/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true, react/sort-prop-types: 0 */
/* globals require, QUnit */
(function () {
    "use strict";    
    
    // Configure RequireJS so it resolves relative module paths from the `src`
	// folder.
    require.config({
        baseUrl: "./src",
		paths: {
			$: "http://code.jquery.com/jquery-2.2.3",
			qunit: "http://code.jquery.com/qunit/qunit-1.23.1",
			delegate: "js/delegate",
			obj: "js/obj",
			dom: "js/dom",
            kit: "js/kit",
			eventbus: "js/eventbus",
			publisher: "js/publisher",
			dispatch: "js/dispatch",
			template: "js/template",
			glossary: "js/glossary",
			storage: "js/storage",
			view: "js/view",
			viewstate: "js/viewstate",
			web: "js/web"
		}
    });

	// A list of all QUnit test Modules.  Make sure you include the `.js` 
	// extension so RequireJS resolves them as relative paths rather than using
	// the `baseUrl` value supplied above.
	var testModules = [
		"tests/dom.tests.js",
		"tests/obj.tests.js",
		"tests/kit.tests.js",
		"tests/publisher.tests.js",
		"tests/eventbus.tests.js",
		"tests/dispatch.tests.js",
		"tests/template.tests.js",
		"tests/glossary.tests.js",
		"tests/storage.tests.js",
		"tests/view.tests.js",
		"tests/viewstate.tests.js",
		"tests/web.tests.js"
	];
	
    // Resolve all testModules and then start the Test Runner.
	require(testModules, function(){
		QUnit.load();
		QUnit.start();
	});
}());

