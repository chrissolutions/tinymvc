/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* globals console, define, Map. Web   */
(function () {
    "use strict";
    
    var dependencies = [
            "qunit",
            "web"
        ];
		
    // Unit test definitions.
	define(dependencies, function(Qunit) {
		
		// Define the QUnit module and lifecycle.
		Qunit.module("Web tests", { 
			setup: function () {
			},
			teardown: function () {
			}
		});
        
        Qunit.test("web request tests", async function( assert ) {
			const data = await Web.get("http://localhost:8081/qtests/test.json");
			console.log(data);
			assert.notEqual(data, null);
        });
		
        Qunit.test("web request html tests", async function( assert ) {
			const data = await Web.get("http://localhost:8081/index.html", {dataType: "text"});
			console.log(data);
			assert.notEqual(data, null);
        });
    });
}());
