/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* globals console, define, delegate, Dispatcher */
(function () {
    "use strict";
    
    var dependencies = [
            "qunit",
            "delegate",
            "eventbus",
            "publisher",
            "dispatch"
        ];
        
    // Unit test definitions.
	define(dependencies, function(Qunit) {
        
		// Define the QUnit module and lifecycle.
		Qunit.module("Dispatch Tests", { 
			setup: function () {
			},
			teardown: function () {
			}
		});
        
        Qunit.test("test dispatch event processing", function( assert ) {
            
            var dispatch = new Dispatch();
            dispatch.on("testMessage", delegate(this, function(assert, testValue) {
                assert.equal(testValue, 4);
            }));
            
            dispatch.trigger("testMessage", assert, 4);
            dispatch.off("testMessage");
            
            // The dispatcher will not trigger this is message as it should be off.
            // The test will fail if triggered.
            dispatch.trigger("testMessage", assert, 5);
        });
        
        Qunit.test("test dispatch command processing", function( assert ) {
            
            const dispatch = new Dispatch(),
                commands = {
                    testCommand: function(assert, testValue) {
                        assert.equal(testValue, 4);
                    }
                };
            
            dispatch.init(commands);
            dispatch.execute(dispatch.commands.testCommand, assert, 4);
        });
    });
}());
