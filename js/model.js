/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* globals console, Dispatch */

/**
 * The base model class.
 *
 * @class
 */
class Model extends Dispatch {

    constructor(controller) {
        super();
        this.on(controller.subscribers);
    }
}
