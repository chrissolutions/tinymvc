/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* eslint no-unused-vars: 0*/
/* globals console, define, delegate, Dom, View  */
(function () {
    "use strict";
    
    var dependencies = [
			"qunit",
            "delegate",
            "eventbus",
            "publisher",
            "dispatch",
			"view"
        ];
		
    // Unit test definitions.
	define(dependencies, function(Qunit) {
		
		class MyView extends View {
			
			constructor() {
				super();
				
				let _viewCommands = {
					
					loadContent: settings => {
						console.log("in loadContent");
					},
					
					showEntries: todos => {
						console.log("in showEntries");
					},
					
					showStats: stats => {
						console.log("in showStats");
					},
					
					toggleAll: isCompleted => {
						console.log("in toggleAll");
					},
					
					setFilter: href => {
						console.log("in setFilter");
					},
					
					clearNewTodo: () => {
						console.log("in clearNewTodo");
					},
					
					completedItem: (id, completed) => {
						console.log("in completedItem");
					},
					
					editItem: (id, title) => {
						console.log("in editItem");
					},
					
					editItemDone: (id, title) => {
						console.log("in editItemDone");
					},
					
					removeItem: id => {
						console.log("in removeItem");
					}
				};
				
				this.init(_viewCommands);
			}
		}
		
		class MyView2 extends View {
			
			constructor(assert) {
				super();
				this.init();
				this.assert = assert;
			}
			
			loadContent(settings) {
				console.log("in MyView2::loadContent");				
				this.assert.equal(settings, "mysettings");

				// Fake event.
				this.trigger(this.messages.todoAdd, "mytitle");
			}
					
			showEntries(todos){
				console.log("in showEntries");
			}
					
			showStats(stats) {
				console.log("in showStats");
			}
			
			toggleAll(isCompleted) {
				console.log("in toggleAll");
			}
			
			setFilter(href) {
				console.log("in setFilter");
			}
			
			clearNewTodo(state) {
				console.log("in MyView2::clearNewTodo");
				this.assert.equal(state, true);
			}
			
			completedItem(id, completed) {
				console.log("in completedItem");
			}
			
			editItem(id, title) {
				console.log("in MyView2::editItem");
				this.assert.equal(id, "myid");
				this.assert.equal(title, "mytitle");

				// Fake event.
				this.trigger(this.messages.todoEditSave, "myid", "mytitle");
			}
			
			editItemDone(id, title) {
				console.log("in MyView2::editItemDone");
				this.assert.equal(id, "myid");
				this.assert.equal(title, "mytitle");
			}
			
			removeItem(id) {
				console.log("in removeItem");
			}
		}
		
		class MyViewChild2 extends View {
			constructor(parent, assert) {
				super(parent);
				this.init();
				this.assert = assert;
			}
			
			loadContent(settings) {
				console.log("in MyViewChild2::loadContent");
				this.assert.equal(settings, "mysettings");

				// Fake event.
				this.trigger(this.messages.todoAdd, "mytitle");
			}
					
			showEntries(todos){
				console.log("in showEntries");
			}
					
			showStats(stats) {
				console.log("in showStats");
			}
			
			toggleAll(isCompleted) {
				console.log("in toggleAll");
			}
			
			setFilter(href) {
				console.log("in setFilter");
			}
			
			clearNewTodo(state) {
				console.log("in MyViewChild2::clearNewTodo");
				this.assert.equal(state, true);
			}
			
			completedItem(id, completed) {
				console.log("in completedItem");
			}
			
			editItem(id, title) {
				console.log("in MyView2::editItem");
				this.assert.equal(id, "myid");
				this.assert.equal(title, "mytitle");

				// Fake event.
				this.trigger(this.messages.todoEditSave, "myid", "mytitle");
			}
			
			editItemDone(id, title) {
				console.log("in MyView2::editItemDone");
				this.assert.equal(id, "myid");
				this.assert.equal(title, "mytitle");
			}
			
			removeItem(id) {
				console.log("in removeItem");
			}
		}
		
		class MyController {
			constructor(assert) {
				this.assert = assert;
				var view = new MyView2(assert);
				
				/**
				 * Subscriber of view events.
				 */
				var subscribers = {
					
					/**
					 * An event to fire whenever you want to add an item. Simply pass in the event
					 * object and it'll handle the DOM insertion and saving of the new item.
					 */
					todoAdd: delegate(this, function (title) {
						console.log("in MyController.subscribers.todoAdd");
						this.assert.equal(title, "mytitle");
						
						// Fake command.
						view.render(view.commands.clearNewTodo, true);
					}),

					/*
					 * Triggers the item editing mode.
					 */
					todoEdit: delegate(this, function (id) {
						console.log("in MyController.subscribers.todoEdit");
						this.assert.equal(id, "myid");
						
						// Fake command.
						view.render(view.commands.editItemDone, id, "mytitle");
					}),
					
					/*
					 * Finishes the item editing mode successfully.
					 */
					todoEditSave: delegate(this, function (id, title) {
						console.log("in MyController.subscribers.todoEditSave");
						this.assert.equal(id, "myid");
						this.assert.equal(title, "mytitle");
						view.render(view.commands.editItemDone, id, title);
					}),

					/*
					 * Cancels the item editing mode and restore previous title.
					 */
					todoEditCancel: delegate(this, function (id) {
						view.render(view.commands.editItemDone, id, "mytitle");
					}),
					
					/**
					 * By giving it an ID it'll find the DOM element matching that ID,
					 * remove it from the DOM and also remove it from storage.
					 *
					 * @param {number} id The ID of the item to remove from the DOM and
					 * storage
					 */
					todoRemove: delegate(this, function (id, silent) {
						view.render(view.commands.removeItem, id);
					}),
					
					/**
					 * Will remove all completed items from the DOM and storage.
					 */
					todoRemoveCompleted: delegate(this, function () {
						console.log("in Controller::todoRemoveCompleted");
					}),
					
					/**
					 * Give it an ID of a model and a checkbox and it will update the item
					 * in storage based on the checkbox's state.
					 *
					 * @param {object} viewdata The checkbox to check the state of complete or not
					 * @param {boolean|undefined} silent Prevent re-filtering the todo items
					 */
					todoToggle: delegate(this, function (viewdata, silent) {
						view.render(view.commands.completedItem);
					}),
					
					/**
					 * Will toggle ALL checkboxes' on/off state and completeness of models.
					 * Just pass in the event object.
					 */
					todoToggleAll: delegate(this, function (completed) {
						console.log("in Controller::todoRemoveCompleted");
					})
				};
				
				view.on(subscribers);
				view.render(view.commands.loadContent, "mysettings");
				
				//Fake command.
				view.render(view.commands.editItem, "myid", "mytitle");
			}
		}
		
		// Define the QUnit module and lifecycle.
		Qunit.module("View tests", { 
			setup: function () {
			},
			teardown: function () {
			}
		});
        
        Qunit.test("verify view commands", function( assert ) {
			let view = new MyView();
            assert.equal(Object.keys(view.commands).length, 10);
			
			let view2 = new MyView2(assert);
			let viewBase = new View();
			const set = new Set();
			Object.getOwnPropertyNames(view2.commands).forEach(m => {
				set.add(m);
			});
			Object.getOwnPropertyNames(viewBase.commands).forEach(m => {
				set.add(m);
			});
			assert.equal(Object.keys(view2.commands).length, set.size);
			
			let viewChild = new MyViewChild2(view2, assert);
			set.clear();
			Object.getOwnPropertyNames(viewChild.commands).forEach(m => {
				set.add(m);
			});
			Object.getOwnPropertyNames(viewBase.commands).forEach(m => {
				set.add(m);
			});

            assert.equal(Object.keys(viewChild.commands).length, set.size);
			assert.equal(view2, viewChild.parent, "verify view parent");
			assert.equal(view2, viewChild.root, "verify view root");
			
			viewChild.name = "viewChild";
            let found = view2.findChildren({name: viewChild.name});
			assert.equal(found.length, 1);
			assert.equal(found[0], viewChild);
			assert.equal(found[0].name, viewChild.name);
		});
		
		Qunit.test("test node", function( assert ) {
			var view = new MyView();
			view.loadContent("qtests"); // doesn't exist.
			assert.ok(view.node, "returns a value");
			assert.ok(view.node instanceof Dom, "node is an instance of Dom");
			assert.equal(view.node, view.ui, "node is equal to ui");
			assert.deepEqual(view.offset, {left: 0, top: 0, width: 0, height: 0}, "zero offsets for non-existing node");

			view.loadContent("qunit"); // exist.
			assert.ok(view.node, "returns a value");
			assert.ok(view.node instanceof Dom, "node is an instance of Dom");
			assert.equal(view.node, view.ui, "node is equal to ui");
			assert.notDeepEqual(view.offset, {left: 0, top: 0, width: 0, height: 0}, "exist: offsets are not zeros");
		});
		
        Qunit.test("test controller", function( assert ) {
			let controller = new MyController(assert);
        });
    });
}());
