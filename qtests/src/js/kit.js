/*jshint strict:true, undef:true, eqeqeq:true, laxbreak:true */
/*global console, Obj */

/**
 * The kit class is a collection that combines sequential access to an association.
 * This was formally called 'Map' but this name collides with the intrinsic JS Map.
 *
 * @class
 */
const Kit = (() => {
    const
        defaultFilter = (query, data) => {
            for (let q of Object.getOwnPropertyNames(query)) {
                if (query[q] !== data[q]) {
                    return false;
                }
            }
            return true;
        };

    class Kit {

        /**
         * constructor
         * 
         * @param {*} json              json object.
         */
        constructor(json) {
            this._entries = !json ? [] : JSON.parse(json);
            this._mapper = Object.create(null);

            // set up the mapper.
            this._entries.forEach((item, index) => {
                this._mapper[item.key] = { "entry": { "key": item.key, "value": item.value }, "index": index };
            });
        }

        /**
         * Gets the collection of map entries.
         *
         * @example
         * map.entries;
         */
        get entries() {
            return this._entries;
        }

        /**
         * Gets the size of the map.
         *
         * @example
         * var count = map.size;
         */
        get size() {
            return this._entries.length;
        }

        /**
         * Gets the key-value pair at a particular index.
         *
         * @param {number} index	The sequence index.
         * @returns {*}             key-value pair.
         *
         * @example
         * map.at(0);
         */
        at(index) {
            return { key: this._entries[index].key, value: this._entries[index].value };
        }

        /**
         * Clears the entries from the map.
         *
         * @example
         * map.clear();
         */
        clear() {
            this._entries = [];
            this._mapper = Object.create(null);
        }

		/**
		 * Finds a collection of matching items on a query given as a JS object
		 *
		 * @param {object} query 		The query to match against (i.e. {foo: 'bar'})
		 * @param {function} filter 	The callback to filter.
         * @returns {Array}             Collection of found items.
		 *
		 * @example
		 * view.find({name: 'viewName'}, function (view) {
		 *	 // data will return any items that have foo: bar and
		*	 // hello: world in their properties
		* });
		*/
        find(query, filter) {

            const results = [],
                select = query || {},
                refine = filter || defaultFilter;

            this._entries.forEach(entry => {
                if (refine(select, entry)) {
                    results.push(entry);
                }
            });

            return results;
        }

        /**
         * Executes a callback for each key/value pair in the map.
         *
         * @param {function} callback   Callback to execute for each element.
         * @param {object} thisArg      The scope of the callback.
         *
         * @example
         * map.forEach(function(value, key, index) {
         *    ...
         * });
         */
        forEach(callback) {
            this._entries.some((entry, index) => {
                return callback(entry.value, entry.key, index);
            });
        }

        /**
         * Gets the value associated with the specified key.
         *
         * @param {string} key          The key of the associated mapped value.
         * @returns {*}                 The value associated to the key.
         *
         * @example
         * var value = map.key("mykey");
         */
        get(key) {
            return this.has(key) ? this._mapper[key].entry.value : undefined;
        }

        /**
         * Determines whether the specified key exists in the map.
         *
         * @param {string} key          The key of the associated mapped value.
         * @returns {boolean}           Returns true if the key exists in the map; otherwise false.
         *
         * @example
         * map.has("mykey");
         */
        has(key) {
            return !!this._mapper[key];
        }

        /**
         * Returns a collection of the map keys.
         *
         * @returns {Array}             Collection of keys.
         * 
         * @example
         * map.keys();
         */
        keys() {
            const results = [];
            for (let ii = 0; ii < this._entries.length; ++ii) {
                results.push(this._entries[ii].keys);
            }
            return results;
        }

        /**
         * Removes entry from the map.
         *
         * @param {string} key          The key of the entry to remove from map.
         *
         * @example
         * map.remove("mystore");
         */
        remove(key) {
            if (this.has(key)) {
                const collection = [];
                let found = 0;
                for (let ii = 0; ii < this._entries.length; ++ii) {
                    if (this._entries[ii].key !== key) {
                        collection.push(this._entries[ii]);
                        this._mapper[this._entries[ii].key].index = ii - found;
                    } else {
                        found = 1;
                    }
                }
                this._entries = collection;
                this._mapper = Obj.omit(this._mapper, key);
            }
        }

        /**
         * Assign a value referenced by a key.
         *
         * @param {string} key          The key of the associated mapped value.
         * @param {object} value        The value to associate with the key.
         *
         * @example
         * map.set(key, value);
         */
        set(key, value) {
            const info = this.has(key)
                ? this._mapper[key]
                : { entry: { "key": key, "value": undefined }, "index": undefined };

            info.entry.value = value;
            let index = info.index;
            if (index !== undefined) {
                this._entries[index] = info.entry;
            } else {
                index = this._entries.length;
                this._entries.push(info.entry);
                info.index = index;
            }

            this._mapper[key] = info;
        }

        /**
         * Returns a collection of the mapped values.
         * 
         * @returns {Array}             Collection of values.
         *
         * @example
         * map.values();
         */
        values() {
            const results = [];
            for (let ii = 0; ii < this._entries.length; ++ii) {
                results.push(this._entries[ii].value);
            }
            return results;
        }

        /**
         * Converts map to JSON string.
         *
         * @returns {String}            the serialized string of the map.
         * 
         * @example
         * map.toString();
         */
        toString() {
            return JSON.stringify(this._entries);
        }
    }

    return Kit;
})();
