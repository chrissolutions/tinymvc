/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* globals console, define, Obj, Kit */
(function () {
    "use strict";
    
    var dependencies = [
            "qunit",
            "obj",
            "kit"
        ],
        
        list = [],
    
        establishKit = function() {
            const kit = new Kit();
            list = [];

            for (var ii = 0; ii < 10; ++ii) {
                var id = Obj.uuid();
                list.push(id);
                kit.set(id, ii);
            }

            return kit;
        };
    
    // Unit test definitions.
	define(dependencies, function(Qunit) {
        
		// Define the QUnit module and lifecycle.
		Qunit.module("Kit tests", { 
			setup: function () {
			},
			teardown: function () {
			}
		});
        
        Qunit.test("create a kit", function( assert ) {
            
            var kit = new Kit();
            assert.notEqual(kit, undefined);
        });
        
        Qunit.test("set 10 items into kit", function(assert) {
            
            var kit = new Kit();
            assert.notEqual(kit, undefined);
            
            for (var ii = 0; ii < 10; ++ii) {
                kit.set(ii, ii);
            }
            
            assert.equal(kit.size, 10);
        });
		
        Qunit.test("find test", function(assert) {
            
            let kit = new Kit();
            assert.notEqual(kit, undefined);
            
            for (let ii = 0; ii < 10; ++ii) {
                kit.set(ii, ii);
            }
            
            assert.equal(kit.size, 10);
			
			// find entry at key 5.
			let entry = kit.find({key: 5})[0];
            assert.equal(entry.key, 5);
            assert.equal(entry.value, 5);
			
			// find all entries.
			let entries = kit.find();
            assert.equal(entries.length, 10);
        });
		
        Qunit.test("foreach loop test", function(assert) {
            
            var kit = new Kit();
            assert.notEqual(kit, undefined);
            
            for (let ii = 0; ii < 10; ++ii) {
                kit.set(ii, ii);
            }
            
            assert.equal(kit.size, 10);
			
			let ii = 0;
			kit.forEach((value, key, index) => {
				console.log(value, key, index);
				ii = index;
				if (key === 5) return true;
			});
			
            assert.equal(ii, 5);
        });
        
        Qunit.test("find kit item of value 4 by id", function(assert) {
            const kit = establishKit();
            var guid = list[4];
            assert.equal(kit.get(guid), 4);
        });
        
        Qunit.test("test value at kit position 4", function(assert) {
            const kit = establishKit();
            assert.equal(kit.at(4).value, 4);
        });
        
        Qunit.test("remove item from kit", function(assert) {
            const kit = establishKit();
            var beforeSize = kit.size;
            var guid = list[4];
            kit.remove(guid);
            assert.equal(kit.size, beforeSize - 1);
            assert.equal(kit.get(guid), undefined);
        });

        Qunit.test("clear the kit", function(assert) {
            const kit = establishKit();
            kit.clear();
            assert.equal(kit.size, 0);
        });
    });
}());
