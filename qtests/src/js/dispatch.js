/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* globals console, EventBus */

/**
 * The dispatch maintains an event bus and command cache
 * for the publishing of events and reception of commands.
 *
 * @class
 */
class Dispatch {
	constructor() {
		this._commands = null,
		this._messages = null;
		this._subscribers = Object.create(null);
		this._commandCache = Object.create(null);
		this._eventBus = new EventBus();
	}

	/**
	 * commands
	 *
	 * getter to access command definitions.
	 */
	get commands() {
		if (!this._commands) {
			this._commands = Object.create(null);
			Object.getOwnPropertyNames(this._commandCache).forEach((command) => {
				this._commands[command] = command;
			});
		}
		return this._commands; 
	}

	/**
	 * messages
	 *
	 * getter to access publishable messages.
	 */
	get messages() {
		if (!this._messages) {
			this._messages = Object.create(null);
			this._eventBus.getMessages().forEach((message) => {
				this._messages[message] = message;
			});
		}
		return this._messages;
	}

	/**
	 * init(commands)
	 *
	 * initializes the dispatcher.
	 *
	 * @param {object}  commands    Command object.
	 */
	init(commands) {
		if (commands) {
			this._commandCache = commands;
		} else {
			let proto = Object.getPrototypeOf(this) || this.__proto__;
            while (proto.constructor.name !== Dispatch.name) {
                const p = proto;
				Object.getOwnPropertyNames(proto).filter(m => proto[m] instanceof Function && proto[m] !== this.constructor).forEach(m => {
					if (!this._commandCache[m]) {
						this._commandCache[m] = p[m];
					}
				});
				proto = Object.getPrototypeOf(proto) || this.__proto__;
			}
		}
	}

    /**
     * Register subscribers 
     * 
     * @param {*} subscribers           event subscribers. 
     */
    register(subscribers) {
		this._subscribers = Object.assign(this._subscribers, subscribers);
    }

	/**
	 * Attaches subscribers to the view.
	 *
	 * @param {string}          message     event message.
	 * @param {function|object} subscriber  subscriber delegate or subscribers object.
	 * 
	 * @example
	 * 	on()						attach collection of registered subscribers
	 * 	on(message, subscriber)		attach a subscriber
	 * 	on(subscribers)				attach collection of subscribers.
	 * 
	 */    
	on(message, subscriber) {
		if (arguments.length === 0) {
			this.on(this._subscribers);
		} else if (typeof message === "string") {
			this._eventBus.subscribe(message, subscriber);
		} else {
			const subscribers = message;
			Object.getOwnPropertyNames(subscribers).forEach(m => {
				this.on(m, subscribers[m]);
			});
		}
	}

	/**
	 * detaches a subscriber or collection of subscribers from the view.
	 *
	 * @param {string}          message     event message.
	 * @param {function|object} subscriber  subscriber delegate or subscribers object.
	 * 
	 * @example
	 * 	off()						detach collection of registered subscribers
	 * 	off(message, subscriber)	detach a subscriber
	 * 	off(subscribers)			detach collection of subscribers.
	 */    
	off(message, subscriber) {
		if (arguments.length === 0) {
			this.off(this._subscribers);
		} else if (typeof message === "string") {
			this._eventBus.unsubscribe(message, subscriber);
		} else {
			const subscribers = message;
			Object.getOwnPropertyNames(subscribers).forEach(m => {
				this.off(m, subscribers[m]);
			});
		}
	}

	/**
	 * trigger(message, [arg1[, arg2[, ...]]])
	 *
	 * triggers a message
	 *
	 * @param {string}      message     event message.
	 * @param {Arguments}   arguments   message arguments.
	 * @returns {*}						event result.
	 */    
	trigger() {
		return this._eventBus.publish.apply(this._eventBus, arguments);
	}

	/**
	 * execute(command, [arg1[, arg2[, ...]]])
	 *
	 * renders a view command.
	 *
	 * @param {string}      command     command message.
	 * @param {Arguments}   arguments   command arguments.
	 */    
	execute(command) {
		[].shift.apply(arguments);
		this._commandCache[command].apply(this, arguments);
	}
}
