/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* eslint no-unused-vars: 0*/
/* globals console, define, delegate, Dom, View  */
(function () {
    "use strict";
    
    var dependencies = [
		"qunit",
		"viewstate"
	];
		
    // Unit test definitions.
	define(dependencies, function(Qunit) {
		
		// Define the QUnit module and lifecycle.
		Qunit.module("ViewState tests", { 
			setup: function () {
			},
			teardown: function () {
			}
		});
        
        Qunit.test("verify view commands", function( assert ) {
			const testObj = {key: "testObj", value: {a: "test value A", b:"test value B", c: "test value C"}};
			const viewstate1 = new ViewState(testObj.key, testObj.value);
			const viewstate2 = new ViewState(testObj.key, testObj.value);
			assert.equal(viewstate1.key, testObj.key, "asset viewstate 1 key");
			assert.equal(viewstate1.value, testObj.value, "asset viewstate 1 value");
			assert.ok(Obj.equals(viewstate1.serialize(), viewstate2.serialize()), "asset serialzed viewstate 1 & 2 are equal");
			assert.ok(Obj.equals(viewstate1.deserialize(), viewstate2.deserialize()), "asset deserialzed viewstate 1 & 2 are equal");
		});
    });
}());
