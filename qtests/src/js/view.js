/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* eslint no-unused-vars: 0 */
/* globals console, Obj, Dom, Kit, Dispatch */
// ReSharper disable UnusedParameter

/**
 * The base view class.
 *
 * @class
 */
const View = (() => {
    const
        viewFilter = (query, data) => {
            for (let q of Object.getOwnPropertyNames(query)) {
                return query[q] === data.value[q];
            }
            return true;
        },

        findRoot = view => {
            let root = view;
            while (root.parent) {
                root = root.parent;
            }
            return root;
        };

    class View extends Dispatch {

        /**
         * View constructor
         * 
         * @param {*} parent            // parent view.
         * @param {*} root              // root view.
         */
        constructor(parent, root) {
            super();
            this._id = Obj.uuid();
            this._name = "";
            this.parent = parent;
            this._root = root || findRoot(this);
            this._children = new Kit();
            this._ui = null;
            if (this.parent) {
                this.parent.addChild(this);
            }
        }

        /**
         * ENTER_KEY
         *
         * @returns {int}               enter key.
         */
        static enterKey() {
            return 13;
        }

        /**
         * ESCAPE_KEY
         *
         * @returns {int}               escape key.
         */
        static escapeKey() {
            return 27;
        }

        /**
         * URI root path.
         *
         * @returns {String}            Base uri.
         */
        get rootUri() {
            return this.node ? this.node.rootUri : "";
        }

        /**
         * View id getter.
         *
         * @returns {String}            Name of the view.
         * 
         * @example
         * var name = view.id;
         */
        get id() {
            return this._id;
        }

        /**
         * View name getter.
         *
         * @returns {String}            Name of the view.
         * 
         * @example
         * var name = view.name;
         */
        get name() {
            return this._name;
        }

        /**
         * View name setter.
         * 
         * @param {string}  value       The view name.
         */
        set name(value) {
            return this._name = value;
        }

        /**
         * Get the view node.
         *
         * @returns {Object}            The underlying UI object.
         * 
         * @example
         * var parent = view.ui;
         */
        get node() {
            return this._node;
        }

        /**
         * Get the offset dimension.
         * 
         * @return {*}                  Offset dimensions of the view.
         */
        get offset() {
            const offset = {left: 0, top: 0, width: 0, height: 0};
            if (this.node && this.node.element) {
                offset.left = this.node.element.offsetLeft;
                offset.top = this.node.element.offsetTop;
                offset.width = this.node.element.offsetWidth;
                offset.height = this.node.element.offsetHeight;
            }
            return offset;
        }

        /**
         * View parent getter & setter.
         *
         * @returns {View}              Parent of the view.
         * 
         * @example
         * var parent = view.parent;
         * this.parent = parentView;
         */
        get parent() {
            return this._parent;
        }

        set parent(view) {
            if (this._parent) {
                this.parent.removeChild(this);
            }

            if (view) {
                view.addChild(this);
            }

            this._parent = view;
            this._root = findRoot(this);
        }

        /**
         * Get the root of the view hierarchy.
         *
         * @returns {View}              The root of the view hierarchy
         * 
         * @example
         * var root = view.root;
         */
        get root() {
            return this._root;
        }

        /**
         * Get the underlying ui object.
         *
         * @returns {Object}            The underlying UI object.
         * 
         * @example
         * var parent = view.ui;
         */
        get ui() {
            return this._ui;
        }

        /**
         * Adds a child to the view.
         *
         * @param {*} view              view to add to children.
         *
         * @example
         * view.addChild(new View('name'));  // Must have a name.
         */
        addChild(view) {
            this._children.set(view.id, view);
        }

		/**
		 * Finds child views based on a query given as a JS object
		 *
		 * @param {*} query 		    query to match against (i.e. {foo: 'bar'})
		 * @param {function} filter 	callback to filter..
         * @returns {Array}             collection of child views.
		 *
		 * @example
		 * view.find({name: 'viewName'}, function (view) {
		 *	 // data will return any items that have foo: bar and
		*	 // hello: world in their properties
		* });
		*/
        findChildren(query, filter) {
            const results = [];

            this._children.find(query, filter || viewFilter).forEach(entry => {
                results.push(entry.value);
            });

            return results;
        }

        /**
         * Gets all of the children of the view.
         *
         * @returns {Array}     Array of view children.
         * 
         * @example
         * var children = view.children;
         */
        getChildren() {
            return this._children.values();
        }

        /**
         * Load content.
         * 
         * @param {string|Dom} nodeId   node identifier.
         * @param {boolean} perserve    perserve underlying underlying ui.
         */
        async loadContent(nodeId, perserve=false) {
            this._node = nodeId instanceof Dom ? nodeId : new Dom(nodeId);
            this._ui = !perserve ? this.node : !this.ui ? this.node : this.ui;
        }

        /**
         * Unload content.
         */
        async unloadContent() {
        }

        /**
         * Remove child view.
         * 
         * @param {View} view           child view.
         */
        removeChild(view) {
            if (view instanceof String) {
                const children = this.findChildren({ name: view.name });

                children.forEach(value => {
                    this._children.remove(value.id);
                });
            } else if (view instanceof View) {
                this._children.remove(view.id);
            }
        }

        /**
         * Render a view command.
         * 
         * @param {Array} args          command arguments.
         * 
         * @example
         * render(command[, arg1[, arg2[, ...]]])
         */
        render(...args) {
            this.execute(...args);
        }

        /**
         * Resize main view.
         * 
         * @param {*} ctx                   window context.
         */
        resize(ctx) {
            return;
        }
    }

    return View;

})();
