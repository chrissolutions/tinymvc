/* jshint strict: true, undef: true, eqeqeq: true */
/* globals Publisher */

/**
 * An event bus is an object that dispatches notification of events to 
 * subscribers that are bound to a specific message.  A message is an 
 * event type.  When an event of a particualar type occurs the event message
 * is published to subscribers of that event type.
 *
 * @class
 *
 * @example
 * var eventBus = new EventBus([messages]);
 */
class EventBus {
    constructor(messages) {
        this._publishers = {};
        if (messages) {
            if (Array.isArray(messages)) {
                messages.forEach((message) => {
                    this._publishers[message] = new Publisher(message);
                });
            } else {
                Object.getOwnPropertyNames(messages)
                    .forEach(message => this._publishers[message] = new Publisher(message));
            }
        }
    }

    /**
     * subscribe(message, subscriber)
     *
     * attaches a subscriber to a message.
     *
     * @param {string}      message     event message.
     * @param {Subscriber}  subscriber  event subscriber.
     */    
    subscribe(message, subscriber) {
        if (!this._publishers[message]) {
            this._publishers[message] = new Publisher(message);
        }
        
        this._publishers[message].subscribe(subscriber);
    }

    /**
     * unsubsribe(message, subscriber)
     *
     * detaches a subscriber from a message.
     *
     * @param {string}      message     event message.
     * @param {Subscriber}  subscriber  event subscriber.
     */    
    unsubscribe(message, subscriber) {
        if (this._publishers[message]) {
            if (!subscriber) {
                this._publishers[message].clear();
            } else {
                this._publishers[message].unsubscribe(subscriber);
            }
        }
    }

    /**
     * publish(message)
     *
     * triggers event to message subscribers.
     *
     * @param {string}      message     event message.
     * @returns {*}                     last subscriber result. 
     */    
    publish(message) {
        if (this._publishers[message]) {
            const args = [];
            for (let ii = 1; ii < arguments.length; ++ii) {
                args.push(arguments[ii]);
            }
            return this._publishers[message].publish.apply(this._publishers[message], args);
        } else {
            return null;
        }
    }
    
    /**
     * getPublishers()
     *
     * returns the event bus publishers 
     *
     * @returns {Array}     Array of publishers.
     */    
    getPublishers() {
        const list = [];
        for (let message in this._publishers) {
            if (this._publishers.hasOwnProperty(message)) {
                list.push(this._publishers[message]);
            }
        }
        return list;
    }

    /**
     * getMessages()
     *
     * returns the event bus message names.
     *
     * @returns {Array}     Array of message names.
     */    
    getMessages() {
        return Object.getOwnPropertyNames(this._publishers);
    }
}
