/*jshint strict: true, undef: true, eqeqeq: true */
/*global $, console, localStorage, Obj, Kit, Messages, View */

/**
 * Creates a storage object that wraps local storage.
 *
 * @param {string} name The name of our DB we want to use
 * @param {function} callback Our fake DB uses callbacks because in
 * real life you probably would be making AJAX calls
 */

const Storage = (() => {
	const asyncResult = result => result instanceof Promise  ? result : new Promise(resolve => resolve(result)),
	defaultFilter = (query, data) => {
		var result = true;
		Object.keys(query).some(q => {
			result = query[q] === data.value[q];
			return !result;
		});
		return result;
	},
	allFilter = () => {
		return true;
	};

	/**
	 * @class
     * @classdesc internal client storage wrapper.
	 */
	class ClientStorage {

		constructor(storage) {
			this._storage = storage || localStorage;
		}

		/**
		 * Return internal storage.
		 */
		get storage() {
			return this._storage;
		}

		/**
		 * Clear internal storage.
         * 
         * @returns {*}                 async result.
		 */
		async clear() {
			return await asyncResult(this.storage.clear());
		}

		/**
		 * Get storage item
		 * @param {*} key				key.
		 * @returns {*} 				item. 
		 */
		async getItem(key) {
			return await asyncResult(this.storage.getItem(key));
		}

		/**
		 * Get number of items in storage.
		 * 
		 * @return {int}				number of items.
		 */
		async length() {
			return await asyncResult(Number.isInteger(this.storage.length) ? this.storage.length : this.storage.length());
		}

		/***
		 * Get the key based on the identifier.
		 * 
		 * @param {*} id				identifier.
		 * @return {*}				    key.
		 */
		async key(id) {
			return await asyncResult(this.storage.key(id));
		}

		/**
		 * Get a collection of keys in the storage.
		 * 
		 * @return {int}				number of items.
		 */
		async keys() {
			return await asyncResult(this.storage instanceof window.Storage ? Object.keys(this.storage) : await this.storage.keys());
		}

        /**
         * Removes the value of a key from the storage.
         * 
         * @param {*} key               key.
         * @returns {*}                 async result.
         */
		async removeItem(key) {
			return await asyncResult(this.storage.removeItem(key));
		}

        /**
         * Set data to the storage.
         * 
         * @param {*} key               key.
         * @param {*} value             value.
         * @returns {*}                 async result.
         */
		async setItem(key, value) {
			return await asyncResult(this.storage.setItem(key, value));
		}
	}

    /**
     * @class
     * @classdesc internal key/value store.
     */
    class Store extends Kit {
		constructor(json, storage, name) {
			super(json);
			this._storage = storage;
			this._name = name;
		}

		/**
		 * Return underlying storage.
		 * 
		 * @returns {ClientStorage}		client storage.
		 */
		get storage() {
			return this._storage;
		}

		/**
		 * Return storage name.
		 * 
		 * @returns {string}			storage name.
		 */
		get name() {
			return this._name;
		}

		/**
		 * Return count of entries in the store.
		 * 
		 * @returns {int}				count of entries in the store.
		 */
		count() {
			return this.size;
		}

		/**
		 * Finds items based on a query given as a JS object
		 *
		 * @param {object} query 		The query to match against (i.e. {foo: 'bar'})
		 * @param {function} filter		filter function.
         * @returns {Array}             array of matched results.
		 *
		 * @example
		 * store.find({foo: 'bar', hello: 'world'});
		*/
		async find(query, filter) {
            const results = [],
                refine = filter || defaultFilter;

			super.find(query, refine).forEach(item => {results.push(item.value);});
			return await asyncResult(results);
		}

		/**
		 * Find all data in storage.
         * 
         * @returns {Array}             array of matched results.
		 */
		async findAll() {
			const results = [];
			(await this.getItems()).forEach(item => {results.push(item.value);});
			return await asyncResult(results);
		}
	
		/**
		 * Returns all storage entries.
         * 
         * @returns {Array}             array of all items in store.
 		 *
		 * @example
		 * storage.getItems();
		 */
		async getItems() {
			return await asyncResult(Array.from(super.entries));
		}
	
		/**
		 * Returns storage entry by its id.
		 *
		 * @param {string|number} id	Entity identifier.
         * @returns {*}             	Entity.
         *
		 * @example
		 * storage.getItem(id);
		 */
		async getItem(id) {
			return await asyncResult(super.get(id));
		}

		/**
		 * Removes and an entity from storage.
		 *
		 * @param {string|number} id	Entity identifier.
		 */
		async removeItem(id) {
			if (super.has(id)) {
				super.remove(id);
				await this.storage.setItem(this.name, this.toString());
			}
		}
		
		/**
		 * Will save the given data to the DB. If no item exists it will create a new
		 * item, otherwise it'll simply update an existing item's properties
		 */
		async save() {
			await this.storage.setItem(this.name, this.toString());
		}
	
		/**
		 * Sets a storage entity.
		 *
		 * @param {Entity} entity		Entity to add to into the storage.
	     * @param {string|number}	[entity.id]		Entity id.
		 *
		 * @example
		 * store.setItem({id: key, data: value});
		 */
		async setItem(entity) {
			if (!entity.id) {
				entity.id = Obj.uuid();
			}
			await asyncResult(super.set(entity.id, entity));
		}
	}
	
	/**
	 * @class
     * @classdesc storage class.
	 *
	 * @example
	 * storage.create("mystore");
	 */
	class Storage {
        constructor(storage) {
			this._map = new Kit();
			this._storage = new ClientStorage(storage);
		}

		get storage() {
			return this._storage;
		}

		/**
		 * Clears all storages and start fresh
		 *
		 * @param {string} name			The name of the storage.
		 */
		async clear() {
			for (let key in this._storage.keys()) {
				await this.drop(key);
			}
		}

		/**
		 * Return count of stores in the storage.
		 * 
		 * @returns {int}			Count of stores in storage.
		 */
		async count() {
			return await this.storage.length();
		}

		/**
		 * Creates the storage.
		 *
		 * @param {string} name    		The name of the storage.
         * @return {Store}              A store assoicated with the name.
		 *
		 * @example
		 * storage.create("mystore");
		 */
		async create(name) {
			const json = await this.storage.getItem(name);		
			const store = new Store(json, this.storage, name);
			this._map.set(name, store);
			return store;
		}

		/**
		 * Drops the store from the storage.
		 *
		 * @param {string} name			The name of the storage.
		 */
		async drop(name) {
			if (await this.storage.getItem(name)) {
				await this.storage.removeItem(name);
			}
			
			if (this._map.has(name)) {
				this._map.get(name).clear();
				this._map.remove(name);
			}
		}

		/**
		 * Finds items based on a query given as a JS object
		 *
		 * @param {string} name 		The name of the storage.
		 * @param {object} query 		The query to match against (i.e. {foo: 'bar'})
		 * @param {function} filter		Filter function.
         * @returns {Array}             Array of matched result.
		 *
		 * @example
		 * db.find("mystore", {foo: 'bar', hello: 'world'});
		*/
		async find(name, query, filter) {
            const results = [],
                refine = filter || defaultFilter,
                store = this._map.get(name);
			
			if (!store) {
				return new Error(`find: storage ${name} not found.`);
			}
			
			store.forEach(value => {
				if (refine(query, value)) {
					results.push(value);
				}
			});
			
			return await asyncResult(results);
		}

		/**
		 * Find all data in storage.
		 *
		 * @param {string} name 		The name of the store.
         * @returns {Array}             Array of all data in the store.
		 */
		async findAll(name) {
			return await this.find(name, Object.create(null), allFilter);
		}

		/**
		 * Get the store associated with the name.
		 *
		 * @param {string} name 		name of the store.
		 * @return {Store}				store associated with the name.
		 */
		async getStore(name) {
			return this._map.has(name)
				? this._map.get(name)
				: await this.create(name);
		}

		/**
		 * Returns all storage entries.
		 *
		 * @param {string} name         name of the store.
         * @returns {Array}             array of all items in store.
		 *
		 * @example
		 * storage.getItems("mystore");
		 */
		async getItems(name) {
			return await asyncResult(Array.from(this._map.get(name).entries));
		}

		/**
		 * Returns storage entry by its id.
		 *
		 * @param {string} name		name of the store.
		 * @param {string} id   	entry identifier.
         * @returns {*}             entry associated with the identifier.
		 *
		 * @example
		 * storage.getItems("mystore");
		 */
		async getItem(name, id) {
			return await asyncResult(this._map.get(name).get(id));
		}

		/**
		 * Determines whether a store exists.
		 *
		 * @param {string} name    name of store.
         * @returns {boolean}      true if store exist; false otherwise.
		 *
		 * @example
		 * storage.has("mystore");
		 */
		has(name) {
			return this._map.has(name);
		}
	}

	return Storage;

})();
