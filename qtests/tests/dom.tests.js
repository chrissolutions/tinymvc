/* jshint strict: true, undef: true, eqeqeq: true, laxbreak: true */
/* eslint eqeqeq: ["error", "smart"] */
/* globals console, define, Dom   */
(function () {
    "use strict";
    
    var dependencies = [
            "qunit",
            "dom"
        ];
        
    // Unit test definitions.
	define(dependencies, function(Qunit) {
        
		// Define the QUnit module and lifecycle.
		Qunit.module("Dom Tests", { 
			setup: function () {
			},
			teardown: function () {
			}
		});
        
        Qunit.test("Dom test", function( assert ) {
            const dom = new Dom(window);
            assert.ok(dom != null, "assert that dom is returned");
            assert.equal(dom.element, window, "assert that element property is equal to window.");
            assert.equal(dom.node, window.document, "assert that node property is equal to document.");

            const find1 = dom.find("qunit");
            assert.ok(find1.element != null, "cannot find qunit div via id");
            assert.ok(find1.element instanceof HTMLElement, "assert result is an html element");

            const find2 = dom.find("#qunit").element;
            assert.ok(find2 !== null, "assert #qunit div via id selector found");
            assert.ok(find2 instanceof HTMLElement, "assert result is an html element");

            const find3 = dom.find(".pass");
            assert.notEqual(find3.element, null, "cannot find .pass divs via class selector");
            assert.notEqual(find3.length, null, "assert multiple .pass divs found via class selector");
            assert.equal(find3.length, find3.element.length, "assert multiple .pass divs found via class selector");
            assert.ok(find3.element instanceof NodeList, "assert nodelist was returned");

            const find4 = dom.find(".content");
            assert.notEqual(find4.element, null, "assert find .content divs via class selector");
            assert.equal(find4.length, 1, "assert only one .content class present in html");
            assert.ok(find4.element instanceof HTMLElement, "assert html element was returned");
        });

        Qunit.test("Dom create test", function( assert ) {
            const html = `<div class="columnlayout">
                            <div class="col-101">
                                <span>Selected Records:&nbsp;</span>
                                <span id = "selectedRecords"></span>
                            </div>
                            <div class="col-102">
                                <span>Total Records:&nbsp;</span>
                                <span id="totalRecords"></span>
                            </div>
                            <div class="col-filler">
                                <span>&nbsp;</span>
                            </div>`;

            const dom = Dom.create(html);
            assert.ok(dom != null, "assert that dom is created and returned");
            assert.equal(dom.element, dom.node, "assert that element property is equal to its node.");

            const find1 = dom.find(".columnlayout");
            assert.ok(find1.element != null, "assert that .columnlayout class is found.");
            assert.equal(find1.element, dom.element.firstElementChild, "assert found node equals first child element of the dom node.");

            const container1 = new Dom();
            const container2 = Dom.create();
            assert.equal(container1.element.childElementCount, 0, "assert empty container has no child elements.");
            assert.equal(container2.node.childElementCount, 0, "assert empty container has no child elements.");
        });

        Qunit.test("Dom root url test", function( assert ) {
            let url = Dom.getRootUri("http://localhost");
            assert.equal(url, "http://localhost", "assert root url is 'http://localhost'.");

            url = Dom.getRootUri("http://localhost/index.html");
            assert.equal(url, "http://localhost", "assert root url of 'http://localhost/index.html' is 'http://localhost'.");

            url = Dom.getRootUri("http://localhost/bridge/index.html");
            assert.equal(url, "http://localhost/bridge", "assert root url of 'http://localhost/bridge/index.html' is 'http://localhost/bridge'.");

            url = Dom.getRootUri("http://localhost/bridge/somefolder/index.html");
            assert.equal(url, "http://localhost/bridge", "assert root url of 'http://localhost/bridge/somefolder/index.html' is 'http://localhost/bridge'.");
        });
    });
}());
