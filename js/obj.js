/* jshint strict:true, undef:true, eqeqeq:true, laxbreak:true */
/* global console */
// ReSharper disable VariableUsedInInnerScopeBeforeDeclared

/**
 * The obj class contains helpers used on objects and values.
 *
 * @class
 */
class Obj {

    /**
     * Convert string source value to data type.
     * 
     * @param {*} source        sourcevalue.
     * @param {*} datatype      target datatype
     * 
     * @returns {*}             value converted to the specified data type.
     */
    static convert(source, datatype) {
        const src = (source || "false").toLowerCase();
        
        switch (datatype) {
            case "boolean": {
                switch (src) {
                    case "0":
                    case "false":
                    case "":
                    case null:
                        return false;

                    default:
                        return true;
                }
            }
            
            case "int":
            case "long":
                return parseInt(!src || src === "false" ? "0" : src === "true" ? "1" : src, 10);

            case "float":
            case "double":
                return parseFloat(!src || src === "false" ? "0" : src === "true" ? "1" : src);

            case "number":
                return Number(!src || src === "false" ? "0" : src === "true" ? "1" : src);

            case "date": {
                const date = moment(source);
                return date.isValid() ? date.format("YYYY-MM-DDTHH:mm:ss.SSSSZ") : null;
            }

            default:
                return source;
        }
    }

    /**
     * Convert string to boolean.
     * 
     * @param {string} source       string source
     * @return {boolean}            boolean value
     */
    static toBoolean(source) {
        return Obj.convert(source, "boolean");
    }

    /**
     * Convert string to number.
     * 
     * @param {string} source       string source
     * @return {number}             numeric value
     */
    static toNumber(source) {
        return Obj.convert(source, "number");
    }

    /**
     * Convert string to int.
     * 
     * @param {string} source       string source
     * @return {int}                integer value
     */
    static toInt(source) {
        return Obj.convert(source, "int");
    }

    /**
     * Convert string to float.
     * 
     * @param {string} source       string source
     * @return {float}              float value
     */
    static toFloat(source) {
        return Obj.convert(source, "float");
    }

    /**
     * Convert string to date.
     * 
     * @param {string} source       string source
     * @return {Date}               date value
     */
    static toDate(source) {
        return Obj.convert(source, "date");
    }

    /**
     * Convert string to date.
     * 
     * @param {string} source       JSON string source
     * @return {*}                  JSON object.
     */
    static toJson(source) {
        return JSON.parse(source);
    }

    /**
     * Omits properties from cloned object.
     * 
     * @param {*} obj               source object.
     * @param {string|Array} keys   list of properties to omit.
     * @returns {Object}            cloned object with omitted properties.
     */
    static omit(obj, keys) {
        const omitKeys = Array.isArray(keys) ? keys : [keys];
        return Object.keys(obj).reduce((result, key) => {
            if (omitKeys.every(k => key !== k)) {
                result[key] = obj[key];
            }
            return result;
        }, {});
    }

    /**
     * Generates a random key.
     * 
     * @param {String} prefix       key prefix.
     * @param {Number} size         key length.
     * 
     * @returns {String}            generated key.
     */
    static genKey(prefix="__key__", size=24) {
        var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var key = prefix;
        var length = size - prefix;
        for (let ii = 0; ii < length; ++ii) {
            key += chars.charAt(Math.round(Math.random() * (chars.length - 1)));
        }
        return key;
    }

    /**
     * Generates and mongo style 24 character object id.
     * 
     * @returns {String}            generated object id.
     */
    static oid() {
        var timestamp = (new Date().getTime() / 1000 | 0).toString(16);
        return timestamp + "xxxxxxxxxxxxxxxx".replace(/[x]/g, () => (Math.random() * 16 | 0).toString(16)).toLowerCase();
    }

    /**
     * Generates unique identifier.
     * 
     * @returns {string}            UUID string.
     */
    static uuid() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            const r = Math.random() * 16 | 0,
                v = c === "x" ? r : r & 0x3 | 0x8;
            return v.toString(16);
        });
    }

    /**
     * Calcuate a hash of an object.
     * 
     * @param {*} obj                   object to hash.
     * @returns {int}                   hash of the object.
     */
    static hash(obj) {
    
        const build = (o) => {
            if (o == null) return 0;
            let hash = 0;
            const str = o.toString();

            for (let ii = 0; ii < str.length; ++ii)
            {
                hash = (hash << 5) - hash + str.charCodeAt(ii) & 0xFFFFFFFF;
            }

            return hash;
        };

        const reflect = o => {
            if (typeof o.getTime === "function") {
                return o.getTime();
            }
        
            let result = 0;
            Object.getOwnPropertyNames(o).forEach(name => {
                result += evaluate(name + build(o[name]));
            });
            return result;
        };

        const evaluate = value => {
            const type = typeof value;
            return typeMap[type](value) + build(type);
        };

        const typeMap = {
            "string": build,
            "number": build,
            "boolean": build,
            "undefined": () => 0,
            "function": () => 0,
            "object": reflect
        };

        return evaluate(obj);
    }

    /**
     * Determines if objects are equal based on their hash.
     * 
     * @param {*} obj1                  object source.
     * @param {*} obj2                  object to compare.
     * @returns {boolean}               true if equal; false if not equal.
     */
    static equals(obj1, obj2) {
        const hash1 = Obj.hash(obj1);
        const hash2 = Obj.hash(Object.assign({}, obj1, obj2));
        return hash1 === hash2;
    }

    /**
     * Test for empty object.
     * 
     * @param {*} obj                   object to test.
     * @returns {boolean}               true if empty; false if not empty.
     */
    static isEmpty(obj) {
        for(var prop in obj)
        {    
            if (obj.hasOwnProperty(prop))
                return false;
        } 
        
        return true;
    }

    /**
     * Test for empty object.
     * 
     * @param {*} obj                   object to test.
     * @returns {boolean}               true if empty; false if not empty.
     */
    static isString(obj) {
        return typeof obj === "string";
    }

    /**
     * Test for empty object.
     * 
     * @param {*} obj                   object to test.
     * @returns {boolean}               true if empty; false if not empty.
     */
    static isObject(obj) {
        return obj != null && typeof obj === "object";
    }
}
